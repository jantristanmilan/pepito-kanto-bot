import trivia_solver as trivia
import time

def main():
    # question = "Which NFL great started his pro career with 10 straight losses?"
    # possible_answers = [
    #   "Brett Favre",
    #   "Dan Marino",
    #   "Troy Aikman"
    # ]

    # question = "What is the term referring to the symbols in comic strips representing swear words?"
    # possible_answers = [
    #   "Plewds",
    #   "Grawlixes",
    #   "Briffits"
    # ]

    question = "Which of these is a common material used in 3D printers?"
    possible_answers = [
      "Durocarbon filament",
      "Polyabsorbic styrene",
      "Polyactic acid"
    ]

    # question = "shanghai disneyland number"
    # possible_answers = [
    #     "5th",
    #     "6th",
    #     "7th"
    # ]

    # question = "The Filipino street food betamax is made of?"
    # possible_answers = [
    #     "Wagyu Blood",
    #     "Chicken Blood",
    #     "Dead Pool 2"
    # ]
    # question = "What are the Bildungsroman genre of stories about?"
    # possible_answers = [
    #     "roman empire",
    #     "coming of age",
    #     "unrequited love"
    # ]

    #question = "How many days does it take for the moon to go around the Earth once?"
    # question = "many days moon around Earth once"
    # possible_answers = [
    #     "27.3",
    #     "18.7",
    #     "30.0"
    # ]
    start = time.time()
    scored_answers = trivia.score_answers(question, possible_answers)
    print(scored_answers)
    print (time.time() - start)


if __name__ == '__main__':
    main()
