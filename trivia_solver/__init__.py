from .vendor_heuristics import wikipedia as wiki
from .vendor_heuristics import google as goog

from operator import add
from text_util import normalize_text

WIKIPEDIA_WEIGHT = 0.85
GOOGLE_WEIGHT = 0.15


def score_answers(question, possible_answers):
    preprocessed_question_tokens = normalize_text(question)
    preprocessed_answers_tokens = [normalize_text(pa) for pa in possible_answers]

    wiki_scores = wiki.compute_scores(preprocessed_question_tokens, possible_answers, preprocessed_answers_tokens)
    weighted_wiki_scores = [WIKIPEDIA_WEIGHT * s for s in wiki_scores]

    if not __is_sufficient_scores(weighted_wiki_scores):
        goog_scores = goog.compute_scores(preprocessed_question_tokens, possible_answers, preprocessed_answers_tokens)
        weighted_goog_scores = [GOOGLE_WEIGHT * s for s in goog_scores]
        return __score_results(possible_answers, list(map(add, weighted_wiki_scores, weighted_goog_scores)))
    else:
        return __score_results(possible_answers, weighted_wiki_scores)


def __is_sufficient_scores(scores):
    return False
    total = sum(scores)
    return total != 0 and total / len(scores) != 1


def __score_results(possible_answers, scores):
    return [{"id": i + 1, "answer": possible_answers[i], "score": score} for i, score in enumerate(scores)]
