import bs4
import re
import requests

from text_util import remove_punctuation

WIKIPEDIA_URL = "https://en.wikipedia.org"
WIKIPEDIA_SEARCH_URL = "https://en.wikipedia.org/wiki/Special:Search?search="
WIKIPEDIA_SEARCH_URL_PARAMS = "&ns0=1&fulltext=Search"

HTML_TAG_REGEX = re.compile('<.*?>')


def compute_scores(preprocessed_question_tokens, possible_answers, preprocessed_answers_tokens):
    try:
        leading_wiki_search_link = __get_leading_wikipedia_search_result_link(preprocessed_question_tokens)
        wiki_article_content = remove_punctuation(__get_wikipedia_article_content(leading_wiki_search_link).lower())
        actual_scores = [__compute_score(possible_answer, wiki_article_content) for possible_answer in possible_answers]
        total = sum(actual_scores)
        return [score / total if total != 0 else score for score in actual_scores]
    except AttributeError:
        return [0 for pa in possible_answers]


def __compute_score(possible_answer, wiki_article_content):
    return wiki_article_content.count(possible_answer.lower()) + 1


def __get_leading_wikipedia_search_result_link(search_tokens):
    search_url = WIKIPEDIA_SEARCH_URL + '+'.join(search_tokens) + WIKIPEDIA_SEARCH_URL_PARAMS
    response = requests.get(search_url)

    if response is not None:
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        leading_search_result = soup.find("div", {"class": "mw-search-result-heading"})
        leading_search_result_link = leading_search_result.findChildren("a", href=True)
        return leading_search_result_link[0]["href"]


def __get_wikipedia_article_content(search_result_link):
    article_url = WIKIPEDIA_URL + search_result_link
    response = requests.get(article_url)

    if response is not None:
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        paragraphs = soup.find("div", {"id": "mw-content-text"}).findAll("p")

        text_content = ""
        for p in paragraphs:
            text_content = text_content + re.sub(HTML_TAG_REGEX, '', p.text)

        return text_content.lower()

