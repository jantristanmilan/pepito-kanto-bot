import bs4
import re
import requests

from text_util import remove_punctuation

GOOGLE_SEARCH_URL = "https://www.google.com/search?q="

HTML_TAG_REGEX = re.compile('<.*?>')


def compute_scores(preprocessed_question_tokens, possible_answers, preprocessed_answers_tokens):
    google_search_results_content = __get_google_search_results_content(preprocessed_question_tokens)
    actual_scores = [__compute_score(possible_answer, preprocessed_answers_tokens[i], google_search_results_content)
                     for i, possible_answer in enumerate(possible_answers)]
    total = sum(actual_scores)
    return [score / total if total != 0 else score for score in actual_scores]


def __compute_score(possible_answer, tokens, google_search_results_content):
    total = google_search_results_content.count(possible_answer.lower()) + 1
    for t in tokens:
        total = total + google_search_results_content.count(t.lower()) + 1
    return total


def __get_google_search_results_content(preprocessed_question_tokens):
    search_url = GOOGLE_SEARCH_URL + "+".join(preprocessed_question_tokens)
    response = requests.get(search_url)

    if response is not None:
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        search_results_body = soup.findAll("span", attrs={"class": "st"})

        content = ""
        for s in search_results_body:
            content = content + remove_punctuation(re.sub(HTML_TAG_REGEX, '', s.text))

        search_result_rows = soup.findAll("h3")
        for srr in search_result_rows:
            content = content + remove_punctuation(re.sub(HTML_TAG_REGEX, '', srr.text))

        search_results_featured = soup.findAll("div", _class="ifM9O")
        for srf in search_results_featured:
            content = content + remove_punctuation(re.sub(HTML_TAG_REGEX, '', srf.text))

        return content.lower()
