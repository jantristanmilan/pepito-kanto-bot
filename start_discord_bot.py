import asyncio
import discord_bot_commands as cmd
import json
import discord_bot_dev_utils as dev

from discord import Game, Role, Member
from discord.ext.commands import Bot
from text_util import normalize_text, markdown_bold, markdown_underline, markdown_code_block, remove_punctuation


BOT_PREFIX = "!"
TOKEN = "NTQ5OTIxNTM1NjQ1NzEyMzg1.D1bDTQ.4hUYU33noPGg3hzrpvNzS47OMeo"  # Get at discordapp.com/developers/applications/me

client = Bot(command_prefix=BOT_PREFIX)


@client.command(name=cmd.DEV_RUN_TEST_SET,
                description=cmd.DEV_RUN_TEST_SET_DESC,
                pass_context=True)
async def on_dev_run_test_set(context, *args):
    for arg in args:
        await __run_test_set(str(arg))


async def __run_test_set(test_data_filename):
    await client.say("Dev testing on the way! Take note, don't let the slow dev testing speed fool you." + \
                     " I am much faster in actual trivia games.")

    test_data_path = "./test_data/" + test_data_filename
    total_success = 0
    with open(test_data_path) as f:
        test_data = json.load(f)
        for t in test_data:
            success = await dev.debug_answer_trivia_set(client, t["question"], t["possible_answers"], t["answer"])
            total_success = total_success + 1 if success else total_success

        bot_success_rate = total_success / len(test_data) * 100

        message = ""
        message = message + "Bot Success Rate = " + str(bot_success_rate) + "%\n"

        blind_guess_success_rate = 1 / len(test_data[0]["possible_answers"]) * 100

        message = message + "Expected Value of Blind Guessing = " + str(blind_guess_success_rate) + "%\n"
        await client.say(markdown_bold(message))


@client.command(name=cmd.DEV_RUN_TEST_ITEM,
                description=cmd.DEV_RUN_TEST_ITEM_DESC,
                pass_context=True)
async def __run_test(context, *args):
    question = args[0]
    possible_answers = args[2:]
    answer = int(args[1])
    await dev.debug_answer_trivia_set(client, question, possible_answers, answer)


@client.event
async def on_ready():
    await client.change_presence(game=Game(name="with humans"))
    print("Logged in as " + client.user.name)


async def list_servers():
    await client.wait_until_ready()
    while not client.is_closed:
        print("Current servers:")
        for server in client.servers:
            print(server.name)
        await asyncio.sleep(600)


client.loop.create_task(list_servers())
client.run(TOKEN)
