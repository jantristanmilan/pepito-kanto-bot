import trivia_solver as trivia

from discord_bot_utils import format_score_answers, sort_highest_score
from text_util import normalize_text, markdown_bold, markdown_underline, markdown_code_block


async def debug_answer_trivia_set(disc_client, question, possible_answers, correct_choice):
    await disc_client.say(markdown_underline("Test Item\n"))
    message = ""

    message = message + "Question = " + question + "\n"
    message = message + "Question Tokens = " + str(normalize_text(question)) + "\n"

    message = message + "Possible Answers = " + str(possible_answers) + "\n"
    for i, pa in enumerate(possible_answers):
        message = message + "Answer " + str(i + 1) + " Tokens = " + str(normalize_text(pa)) + "\n"

    score_answers = trivia.score_answers(question, possible_answers)
    sorted_score_answers = sort_highest_score(score_answers)
    success = sorted_score_answers[0]["id"] == correct_choice

    message = message + "Result = " + ("SUCCESS" if success else "FAIL") + "\n"
    message = message + "Expected = " + possible_answers[correct_choice - 1] + "\n"
    message = message + "Actual = " + sorted_score_answers[0]["answer"] + "\n"
    message = message + format_score_answers(sorted_score_answers) + "\n"

    await disc_client.say(markdown_code_block(message))
    return success
