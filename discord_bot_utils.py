from operator import itemgetter


def format_score_answers(score_answers):
    message = ""
    for sa in score_answers:
        message = message + str(sa["id"]) + ") " + sa["answer"] + " = " + str(sa["score"] * 100) + "\n"

    return message


def sort_highest_score(score_answers):
    return sorted(score_answers, key=itemgetter('score'), reverse=True)
