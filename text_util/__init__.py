from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

RELATIVE_PRONOUNS = ["what", "when", "who", "which", "whom", "where", "why", "how"]
STOP_WORDS = set(stopwords.words('english')) - {"most", "least", "not", "after"}


def normalize_text(text):
    text = remove_punctuation(text)
    tokens = word_tokenize(text.lower())

    keyword_tokens = [t for t in tokens if t not in RELATIVE_PRONOUNS and t not in STOP_WORDS]

    # Stemming does not seem to work well
    # stemmer = PorterStemmer()
    # stemmed_keyword_tokens = [stemmer.stem(t) for t in keyword_tokens]

    return keyword_tokens


def remove_punctuation(text):
    punctuation = ',?!()'
    for symbol in punctuation:
        text = text.replace(symbol, ' ')

    return text


def markdown_bold(text):
    return "**" + text + "**"


def markdown_underline(text):
    return "__" + text + "__"


def markdown_code_block(text):
    return "```" + text +  "```"
