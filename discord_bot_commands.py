# User commands

SHOW_MAN_PAGE = "man"
SHOW_MAN_PAGE_DESC = "Shows the manual page on how to use the discord bot."

# Developer specific commands

DEV_RUN_TEST_SET = "dev_test_set"
DEV_RUN_TEST_SET_DESC = "Starts a test for selected dataset."


DEV_RUN_TEST_ITEM = "dev_test_item"
DEV_RUN_TEST_ITEM_DESC = "Starts a test for the arguments supplied."
